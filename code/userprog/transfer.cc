/// Copyright (c) 2019-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.


#include "transfer.hh"
#include "lib/utility.hh"
#include "threads/system.hh"


void ReadBufferFromUser(int userAddress, char *outBuffer,
												unsigned byteCount)
{
	ASSERT(userAddress != 0);
	ASSERT(outBuffer != nullptr);
	ASSERT(byteCount != 0);

	unsigned count = 0;
	do {
		int temp;
		count++;
		DEBUG('v', "Reading in %u\n", userAddress);
		for (int i = 0; ; i++) {
			DEBUG('v', "Try: %d in %u\n", i, userAddress);
			if (machine->ReadMem(userAddress, 1, &temp)) break;
			else {
				DEBUG('v', "Try: %d in %u missed\n", i, userAddress);
				if (i > 4) ASSERT(false);
			}
		}
		userAddress++;
		*outBuffer = (unsigned char) temp;
		outBuffer++;
	} while (count < byteCount);
}

bool ReadStringFromUser(int userAddress, char *outString,
												unsigned maxByteCount)
{
	ASSERT(userAddress != 0);
	ASSERT(outString != nullptr);
	ASSERT(maxByteCount != 0);

	unsigned count = 0;
	do {
		int temp;
		count++;
		DEBUG('v', "Reading in %u\n", userAddress);
		for (int i = 0; ; i++) {
			DEBUG('v', "Try: %d in %u\n", i, userAddress);
			if (machine->ReadMem(userAddress, 1, &temp)) break;
			else {
				DEBUG('v', "Try: %d in %u missed\n", i, userAddress);
				if (i > 4) ASSERT(false);
			}
		}
		userAddress++;
		*outString = (unsigned char) temp;
	} while (*outString++ != '\0' && count < maxByteCount);

	return *(outString - 1) == '\0';
}

void WriteBufferToUser(const char *buffer, int userAddress,
											 unsigned byteCount)
{
	ASSERT(userAddress != 0);
	ASSERT(buffer != nullptr);
	ASSERT(byteCount != 0);

	unsigned count = 0;
	do {
		int temp = buffer[count];
		count++;
		DEBUG('v', "Writing in %u\n", userAddress);
		for (int i = 0; ; i++) {
			if (machine->WriteMem(userAddress, 1, temp)) break;
			else if (i > 4) {
				ASSERT(false);
			}
		}
		userAddress++;
	} while (count < byteCount);

	return;
}

void WriteStringToUser(const char *string, int userAddress)
{
	ASSERT(userAddress != 0);
	ASSERT(string != nullptr);

	do {
		int temp = (int)*string;
		DEBUG('v', "Writing in %u\n", userAddress);
		for (int i = 0; ; i++) {
			if (machine->WriteMem(userAddress, 1, temp)) break;
			else if (i > 4) {
				ASSERT(false);
			}
		}
		userAddress++;
	} while (*string++ != '\0');

	return;
}
