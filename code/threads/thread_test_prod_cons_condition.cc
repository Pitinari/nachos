/// Copyright (c) 1992-1993 The Regents of the University of California.
///               2007-2009 Universidad de Las Palmas de Gran Canaria.
///               2016-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.


#include "thread_test_prod_cons_condition.hh"
#include "../lib/list.hh"
#include "lock.hh"
#include "condition.hh"

#include <stdio.h>

#define BUFFER_SIZE 3
#define NUM_PRODUCERS 3
#define NUM_CONSUMERS 3

int *buffer[BUFFER_SIZE];
int cantidad = 0;
int espacio = BUFFER_SIZE;
Lock *lock;
Condition *libre, *ocupado;

void send(int *p) {
	lock->Acquire();
	while (espacio == 0) libre->Wait();
	buffer[cantidad] = p;
	cantidad++;
	espacio--;
	ocupado->Signal();
	lock->Release();
	return;
}

int *receive() {
	lock->Acquire();
	while (espacio == BUFFER_SIZE) ocupado->Wait();
	cantidad--;
	int *p = buffer[cantidad];
	espacio++;
	libre->Signal();
	lock->Release();
	return p;
}

void
ProducerThreadCond(void *arg) {
	DEBUG('t',"%s producer awake\n", currentThread->GetName());
	while (true) {
		int *p = new int;
		*p = *(int*) arg;
		send(p);
		currentThread->Yield();
	}
}

void
ConsumerThreadCond(void *arg) {
	DEBUG('t',"%s consumer awake\n", currentThread->GetName());
	while(true) {
		int *p = receive();
		printf("%d consumed by %s\n", *p, currentThread->GetName());
		delete p;
		currentThread->Yield();
	}
}

void
ThreadTestProdConsCond()
{
	DEBUG('t', "Initializating...\n");
	lock = new Lock("Producer/Consumer Buffer Lock");
	libre = new Condition("Producer/Consumer Free Condition", lock);
	ocupado = new Condition("Producer/Consumer Ocuppied Condition", lock);

	DEBUG('t', "Waking up producers...\n");
	for(int i = 0; i < NUM_PRODUCERS; i++) {
		char *name = new char[10];
		sprintf(name, "%dth", i+1);
		int *arg = new int;
		*arg = i+1;
		Thread *producer = new Thread(name);
		producer->Fork(ProducerThreadCond, (void*)arg);
	}

	DEBUG('t', "Waking up consumers...\n");
	for(int i = 0; i < NUM_CONSUMERS; i++) {
		char *name = new char[10];
		sprintf(name, "%dth", i+1+NUM_PRODUCERS);
		void *arg = name;
		Thread *consumer = new Thread(name);
		consumer->Fork(ConsumerThreadCond, arg);
	}

	while (true) {
		currentThread->Yield();
	}    
}
