#ifndef NACHOS_CHANNEL__HH
#define NACHOS_CHANNEL__HH

#include "semaphore.hh"

class Channel {
public:
  Channel();
  ~Channel();
  void Send(int message);
  void Receive(int *message);
private:
  int data;
  Semaphore *sender;
  Semaphore *receiver;
  Semaphore *finish;
};

#endif