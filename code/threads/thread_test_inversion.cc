
#include "thread_test_inversion.hh"
#include "system.hh"
#include <stdio.h>
#include "lock.hh"

Lock* l;

void High(void* args) {
    l->Acquire();
    l->Release();

    printf("High priority task done.\n");
}

void Medium(void* args) {
    printf("Medium priority infinite loop...\n");

    while (1) currentThread->Yield();
}

void Low(void* args) {
    l->Acquire();
    currentThread->Yield();
    l->Release();

    printf("Low priority task done.\n");
}

void ThreadTestInversion() {

#ifndef INVERSION
    printf("The priority inversion flag isn't activated.\n");
#else
    printf("The priority inversion flag is activated.\n");
#endif

    l = new Lock("Lock");

    Thread *t3 = new Thread("High", false, MAX_PRIORITY);
    Thread *t2 = new Thread("Medium 2", false, 3);
    Thread *t1 = new Thread("Low", false, 0);
    t1->Fork(Low, nullptr);
    currentThread->Yield();
    t2->Fork(Medium, nullptr);
    t3->Fork(High, nullptr);
    currentThread->Yield();
}