/// Copyright (c) 1992-1993 The Regents of the University of California.
///               2007-2009 Universidad de Las Palmas de Gran Canaria.
///               2016-2021 Docentes de la Universidad Nacional de Rosario.
/// All rights reserved.  See `copyright.h` for copyright notice and
/// limitation of liability and disclaimer of warranty provisions.


#include "thread_test_prod_cons.hh"
#include "../lib/list.hh"
#include "lock.hh"
#include "condition.hh"

#include <stdio.h>

#define BUFFER_SIZE 10
#define NUM_PRODUCERS 3
#define NUM_CONSUMERS 3

template <class Item>
class Buffer {
  public:
    Buffer(const char *name);

    ~Buffer();

    void Send(Item item);

    Item Receive();

  private:
    List<Item> *buffer;
    unsigned numElems = 0;
    Lock *listLock;
    Condition *senderCondition;
    Condition *receiverCondition;
};

template <class Item>

Buffer<Item>::Buffer(const char *name) {
  buffer = new List<Item>;
  listLock = new Lock("Buffer lock");
  senderCondition = new Condition("Buffer sender condition", listLock);
  receiverCondition = new Condition("Buffer receiver condition", listLock);
}

template <class Item>
Buffer<Item>::~Buffer() {
  delete senderCondition;
  delete receiverCondition;
  delete listLock;
  delete buffer;
}

template <class Item>
void
Buffer<Item>::Send(Item item) {
  listLock->Acquire();
  ASSERT(numElems <= BUFFER_SIZE);
  while(numElems == BUFFER_SIZE){
    senderCondition->Wait();
  }
  buffer->Append(item);

  if(numElems == 0) {
    receiverCondition->Signal();
  }

  numElems++;
  listLock->Release();
}

template <class Item>
Item 
Buffer<Item>::Receive() {
  listLock->Acquire();
  ASSERT(numElems <= BUFFER_SIZE);
  while(numElems == 0){
    receiverCondition->Wait();
  }
  Item item = buffer->Pop();

  if(numElems == BUFFER_SIZE) {
    senderCondition->Signal();
  }

  numElems--;
  listLock->Release();

  return item;
}


void
ProducerThread(void *arg) {
    Buffer<char *> *buffer = (Buffer<char *> *) ((void **)arg)[0];
    char *name = (char *) ((void **)arg)[1];
    DEBUG('t',"%s producer awake\n", currentThread->GetName());
    while(true) {
        buffer->Send(name);
        currentThread->Yield();
    }
}

void
ConsumerThread(void *arg) {
    Buffer<char *> *buffer = (Buffer<char *> *) ((void **)arg)[0];
    DEBUG('t',"%s consumer awake\n", currentThread->GetName());
    while(true) {
        printf("%s consumed by %s\n", buffer->Receive(), currentThread->GetName());
        currentThread->Yield();
    }
}

void
ThreadTestProdCons()
{
    DEBUG('t', "Initializating...\n");
    Buffer<char *> *buffer = new Buffer<char *>("Producer/Consumer Test Buffer");

    DEBUG('t', "Waking up producers...\n");
    for(int i = 0; i < NUM_PRODUCERS; i++) {
        char *name = new char[10];
        sprintf(name, "%dth", i+1);
        void **arg = (void **) new char[sizeof(void*) * 2];
        arg[0] = (void *) buffer;
        arg[1] = (void *) name;
        Thread *producer = new Thread(name);
        producer->Fork(ProducerThread, arg);
    }

    DEBUG('t', "Waking up consumers...\n");
    for(int i = 0; i < NUM_CONSUMERS; i++) {
        char *name = new char[10];
        sprintf(name, "%dth", i+1+NUM_PRODUCERS);
        void *arg[2];
        arg[0] = (void *) buffer;
        arg[1] = (void *) name;
        Thread *consumer = new Thread(name);
        consumer->Fork(ConsumerThread, arg);
    }

    while (true) {
        currentThread->Yield();
    }
    
}
