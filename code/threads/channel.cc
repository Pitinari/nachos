#include "channel.hh"

Channel::Channel() {
  sender = new Semaphore("Sender", 1);
  receiver = new Semaphore("Receiver", 0);
  finish = new Semaphore("Finished", 0);
}

Channel::~Channel() {
  delete sender;
  delete receiver;
  delete finish;
}

void
Channel:: Send(int message) {
  sender->P();
  data = message;
  receiver->V();
  finish->P();
  sender->V();
}

void
Channel:: Receive(int *message) {
  receiver->P();
  *message = data;
  finish->V();
}
