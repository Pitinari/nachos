#include "syscall.h"
#include "lib.c"

int main(int argc, char* argv[]) {
  if (argc == 1) {
    myPuts("cp: missing file operand");
    return -1;
  }
  else if (argc == 2) {
    myPuts("cp: missing destination file operand after");
    return -1;
  }
  else if (argc != 3) {
    myPuts("cp: invalid arguments count");
    return -1;
  }
  char *src = argv[1];
  OpenFileId srcId = Open(src);
  if (srcId < 0) {
    myPuts("cp: could not open src file");
    return -1;
  }
  char *dest = argv[2];
  if (Create(dest) == -1) {
    myPuts("cp: could not create dest file");
    return -1;
  }
  OpenFileId destId = Open(dest);
  char buffer[60];
  int tmp;
  while ((tmp = Read(buffer, 60, srcId)) > 0) {
    Write(buffer, tmp, destId);
  }
  Close(srcId);
  Close(destId);
  return 0;
}