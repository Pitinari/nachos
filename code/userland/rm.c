#include "syscall.h"
#include "lib.c"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    myPuts("rm: missing operand");
    return -1;
  }
  for (int i = 1; i < argc; i++) {
    char *filename = argv[i];
    if (Remove(filename)) {
      myPuts("rm: cannot remove: No such file or directory");
    }
  }
  return 0;
}