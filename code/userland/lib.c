#include "syscall.h"

unsigned strlen(const char *s) {
  unsigned i = 0;
  while (*(s+i) != '\0') {
    i++;
  }
  return i;
}

void myPuts(const char *s) {
  OpenFileId output = CONSOLE_OUTPUT;

  Write(s, strlen(s), output);
  Write("\n", 1, output);
}

void reverse(char* str, int len) {
  for (int i = 0; i < len / 2; i++) {
    char aux = str[i];
    str[i] = str[len - i - 1];
    str[len - i - 1] = aux;
  }
}

void itoa(int n, char *str) {
  int i = 0;
  if (n < 0) {
    str[i++] = '-';
    n = -n;
  }
  while (n > 0) {
    str[i++] = '0' + (n % 10);
    n /= 10;
  }
  str[0] == '-' ? reverse(str+1, i-1) : reverse(str, i);
  str[i] = '\0';
}
