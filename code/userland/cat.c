#include "syscall.h"
#include <stdio.h>
#include "lib.c"

int main(int argc, char* argv[]) {
  char buffer[1];
  OpenFileId output = CONSOLE_OUTPUT;

  if (argc < 2) {
    OpenFileId input = CONSOLE_INPUT;
    for (;;) {
      Read(buffer, 1, input);
      Write(buffer, 1, output);
    }
    return 0;
  }
  
  char *filename;
  for (int i = 1; i < argc; i++) {
    filename = argv[i];
    OpenFileId file = Open(filename);
    if (file < 2) {
      myPuts("cat: No such file or directory");
    }
    else {
      while (Read(buffer, 1, file) > 0) {
        Write(buffer, 1, output);
      }
      Close(file);
    }
  }
  
  return 0;
}